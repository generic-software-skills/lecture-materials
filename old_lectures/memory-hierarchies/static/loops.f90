program loop_exchange
   implicit none
   integer, parameter :: nel = 20000
   ! Note: Stay below 46000 to prevent overflows below
   integer, dimension(nel, nel) :: mat
   integer :: i, j

   do i = 1, nel
      do j = 1, nel
         mat(j, i) = (i-1)*nel + j-1
      end do
   end do
end program loop_exchange

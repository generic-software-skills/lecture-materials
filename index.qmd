---
title: "Generic Software Skills"
---

The course will take place during Summer Semester 2025, **Tuesdays** at **13:00** to **15:00** in **room 1536a** in Bundesstraße 55.
If you have a portable computer at hand, please bring it to the lecture.
If not, don't worry, the lecture will be in a computer room.

## Lectures
| Date | Title | Lecturers |
| --- | --- | --- |
| 2025-04-08 | The command line | Lukas |
| 2025-04-15 | Git | Lukas |
| 2025-04-22 | coding environment & reproducibility | Lukas |
| 2025-04-29 | Tooling & CI | Manuel |
| 2025-05-06 | Testing |  Flo |
| 2025-05-13 | Refactoring & legacy code, facade pattern | Manuel |
| 2025-05-20 | Debugging (Strategies) | Manuel |
| [2025-05-27]{.inactive} | (Pentecost break) | - |
| 2025-06-03 | User experience design | Lukas |
| 2025-06-10 | error handling & observability (& logging) | Flo|
| 2025-06-17 | Data structures | Tobi |
| 2025-06-24 | Complexity | Tobi |
| 2025-07-01 | Programming paradigms | Tobi |
| 2025-07-08 | Git advanced | Tobi |
| 2025-07-15 | Open Development, Licenses, Code of Conduct, Contributing etc... | Lukas |

Subscribe to [iCal]({{< env ICAL_URL >}})

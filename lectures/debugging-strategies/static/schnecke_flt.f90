program schnecke

  ! The code flags 42 pairs of (i,j) indices as True
  ! and relocates these in a larger domain by starting
  ! at index (is,js)


  implicit none

  Integer, parameter :: m    = 1
  Integer, parameter :: idim = 128
  Integer, parameter :: jdim = 64

  Type memo
     Integer :: i
     Integer :: j
     Integer :: dir
  End Type

  Integer    :: i,  j
  Integer    :: index, nn
  Integer    :: is, js

  Integer    :: stacksize
  Integer    :: current_stacksize

  Integer    :: nbr_cells_tot
  Integer    :: nbr_cells_inc

  Type (memo), Pointer :: stack(:)
  Type (memo), Pointer :: new_stack(:)

  Integer              :: nbr_cells(m)

  Integer, Pointer     :: tmp_neighcells_3d(:,:)
  Integer, Pointer     :: new_neighcells_3d(:,:)

  Logical    :: cyclic(2)

  Logical    :: flagged(idim,jdim)
  Integer    :: visited(idim,jdim)

  Integer    :: direction(2,4)

  data direction / -1, 0, 0, -1, 1, 0, 0, 1 /

!
! Initialise variables for test case
!-----------------------------------

  cyclic = .false.

  current_stacksize = 0

  flagged = .false.

  do j = 45, 50
     do i = 30, 36
        flagged(i,j) = .true.
     enddo
  enddo

  is = 34
  js = 48
!
! ----------------------------------
!
  stacksize     = 4
  nbr_cells_inc = 4
  nbr_cells_tot = nbr_cells_inc

  allocate(stack(stacksize))

  allocate(tmp_neighcells_3d(2,nbr_cells_tot))

  visited   = 0
  nbr_cells = 0

  !
  ! Initialise first stack entry
  !
  index = 1
  stack(index)%i   = is
  stack(index)%j   = js
  stack(index)%dir = 0
  !
  ! ... maybe we should test first whether our initial guess is really good
  !
  visited(is,js) = m

  nn = 1

  tmp_neighcells_3d(1,nn) = stack(index)%i
  tmp_neighcells_3d(2,nn) = stack(index)%j

  nbr_cells(1) = 1
  !
  ! Andreas Algorithmus
  !
  do while ( index > 0 )
     !
     ! Change direction
     !
     stack(index)%dir = stack(index)%dir + 1

     if ( stack(index)%dir > 4 ) then
        !
        ! All 4 directions are already tested
        ! for this cell so we have to go back
        !
        index = index - 1
        cycle
     endif
     !
     ! Go to the next cell
     !
     is = stack(index)%i + direction(1,stack(index)%dir)
     js = stack(index)%j + direction(2,stack(index)%dir)
     !
     ! Apply cyclic boundary conditions if required
     !
     if ( cyclic(1) ) is = mod(is-1,idim)+1
     if ( cyclic(2) ) js = mod(js-1,jdim)+1
     !
     ! Check that indices are in the allowed range
     !
     if ( is < 1 .or. is > idim ) cycle
     if ( js < 1 .or. js > jdim ) cycle
     !
     ! When cell is already checked for this target ...
     !
     if ( visited(is,js) == m ) cycle
     !
     ! Test current cell
     !
     if ( .not. flagged(is,js) ) then
        !
        ! Mark cell as checked for this cycle
        !
        visited(is,js) = m
        cycle

     else
        !
        ! We found a new source cell
        !
        nbr_cells(m) = nbr_cells(m) + 1
        !
        ! ... allocate new memory if necessary
        !
        if ( nn + 1 > nbr_cells_tot ) then
            nbr_cells_tot = nbr_cells_tot + nbr_cells_inc
            allocate(new_neighcells_3d(2,nbr_cells_tot))
            new_neighcells_3d(1:2,1:nn) = tmp_neighcells_3d(1:2,1:nn)
            deallocate(tmp_neighcells_3d)
            tmp_neighcells_3d => new_neighcells_3d
        endif

        nn = nn + 1

        tmp_neighcells_3d(1,nn) = is
        tmp_neighcells_3d(2,nn) = js
        !
        ! Prepare for the next source cell to test
        !
        ! ... allocate new memory for the stack if necessary
        !
        if ( index + 1 > current_stacksize ) then
           current_stacksize =  stacksize
           allocate(new_stack(current_stacksize))
           new_stack(1:index) = stack(1:index)
           deallocate(stack)
           stack => new_stack
        endif

        index = index + 1
        stack(index)%i   = is
        stack(index)%j   = js
        stack(index)%dir = 0

        visited(is,js) = m

     endif

  enddo
  !
  ! Finally shrink tmp_neighcells_3d to required size
  !
  if ( nn < nbr_cells_tot ) then
     nbr_cells_tot = nn
     allocate(new_neighcells_3d(2,nbr_cells_tot))
     new_neighcells_3d(1:2,1:nn) = tmp_neighcells_3d(1:2,1:nn)
     deallocate(tmp_neighcells_3d)
     tmp_neighcells_3d => new_neighcells_3d
  endif
  !
  ! Test
  !
  do i = 1, nbr_cells_tot
     if ( tmp_neighcells_3d(2,i) < 45 .or. tmp_neighcells_3d(2,i) > 50 .or. &
          tmp_neighcells_3d(1,i) < 30 .or. tmp_neighcells_3d(1,i) > 36 ) then
        print *, 'WARNING at i ', i, tmp_neighcells_3d(1,i), tmp_neighcells_3d(2,i)
     endif
     print *, ' Found ', tmp_neighcells_3d(1,i), tmp_neighcells_3d(2,i)
  enddo

  print *, ' Found ', nbr_cells, ' cells.'

end program schnecke



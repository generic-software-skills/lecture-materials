#!/usr/bin/env python3

"""
Simple approximation of the golden ratio by taking the ratio of
two subsequent fibonacci numbers
"""


def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)


def golden_ratio(n):
    return fibonacci(n) / fibonacci(n - 1)


if __name__ == "__main__":
    print(f"{golden_ratio(35)=}")

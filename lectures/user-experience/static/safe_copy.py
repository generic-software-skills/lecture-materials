#!/usr/bin/env python3

import argparse
import shutil
import os


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-I", "--outfIle", help="Where to copy")
    parser.add_argument("-O", "--sOurce", help="Source file")
    args = parser.parse_args()
    return args


def copy_file(args):
    if not os.access(args.outfIle, os.F_OK):
        shutil.copyfile(args.sOurce, args.outfIle)


if __name__ == "__main__":
    args = parse_args()
    copy_file(args)

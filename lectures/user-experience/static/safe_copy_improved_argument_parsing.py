#!/usr/bin/env python3

import argparse
import shutil
import os


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="Where to copy from")
    parser.add_argument("outfile", help="Where to copy to")
    args = parser.parse_args()
    return args


def copy_file(infile, outfile):
    if not os.access(outfile, os.F_OK):
        shutil.copyfile(infile, outfile)


if __name__ == "__main__":
    args = parse_args()
    copy_file(args.infile, args.outfile)

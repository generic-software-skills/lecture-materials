#!/usr/bin/env python3

import argparse
import shutil
import os
import logging
import sys


class FileExists(Exception):
    def __init__(self, filename):
        self.filename = filename

    def __str__(self) -> str:
        return f"File already exists: {self.filename}"


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="Where to copy from")
    parser.add_argument("outfile", help="Where to copy to")
    args = parser.parse_args()
    return args


def copy_file(infile, outfile):
    if not os.access(outfile, os.F_OK):
        shutil.copyfile(infile, outfile)
    else:
        raise FileExists(filename=outfile)


def log_error(args, error):
    logging.error(f"Could not copy {args.infile} to {args.outfile}: {error}")


if __name__ == "__main__":
    args = parse_args()
    try:
        copy_file(args.infile, args.outfile)
    except OSError as ose:
        if (ose.errno) == 2:
            outdir = os.path.dirname(ose.filename)
            if outdir and not os.path.isdir(outdir):
                logging.error(
                    f"Could not copy {args.infile} to {args.outfile}: {outdir} is not a directory."
                )
                sys.exit(1)
        log_error(args, ose)
        sys.exit(1)
    except Exception as e:
        log_error(args, e)
        sys.exit(1)

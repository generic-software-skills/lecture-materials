#!/usr/bin/env python3

import argparse
import shutil
import os


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--infile", help="Where to copy from", required=True)
    parser.add_argument("-o", "--outfile", help="Where to copy to", required=True)
    args = parser.parse_args()
    return args


def copy_file(args):
    if not os.access(args.outfile, os.F_OK):
        shutil.copyfile(args.infile, args.outfile)


if __name__ == "__main__":
    args = parse_args()
    copy_file(args)

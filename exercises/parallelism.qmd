---
title: "Parallelism"
---
### Tasks:
  1. Revisit `schedule` and try `schedule(dynamic,100)` as in the hands-on and
     explain what happens and why that differs from `schedule(static, 100)`.
  42. Parallelize the loop in `maxval`. Search for the comment
     `// HOMEWORK: Parallelise this loop!`. All information needed can be found
     in the lecture slides.
  23.  Do a strong-scaling experiment starting with 2 threads and up to 32
      threads and plot the result.
  42. If you were to increase the number of threads, do you expect the speedup
      to continue indefinitely? If not, which limits can you imagine? Feel free
      to use kitchen metaphors.

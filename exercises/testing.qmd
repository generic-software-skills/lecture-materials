---
title: "Testing"
---

## Exercise
* Build a Gitlab-CI for your personal project
  * E.g. run the python files in the repository
* Add comments to the `.gitlab-ci.yaml` to explain your thoughts

> Tipp: You need to enable CI/CD in Settings->General->"Visibility, project features, permissions" and enable enable instance runners under "Settings->CI/CD->Runners".

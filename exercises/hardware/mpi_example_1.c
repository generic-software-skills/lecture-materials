#include <stdio.h>
#include <math.h>
#include <mpi.h>           // include the header for the MPI library

int main(int argc, char** argv){

   int no_of_ranks, my_rank, n_per_rank;

   int n = 10;

   int my_first, my_last;


   MPI_Init(&argc, &argv); // initiate MPI computation

   MPI_Comm_size(MPI_COMM_WORLD, &no_of_ranks); // get number of MPI ranks

   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);  // get rank of this process

   // Calculate the number of iterations per rank
   n_per_rank = floor(n/no_of_ranks);
   if( n%no_of_ranks > 0 ){
      // Add 1 in case the number of ranks doesn't divide n
      n_per_rank += 1;
   }

   // Figure out the first and the last iteration for this rank
   my_first = my_rank * n_per_rank;
   my_last = my_first + n_per_rank;

   // Run only the part of the loop this rank needs to run
   // The if statement makes sure we don't go over
   for( int i=my_first; i<my_last; i++ ) {
      if( i < n ) {
         printf("I am process %d and I am printing the number %d.\n", my_rank, i);
      }
   }

   MPI_Finalize();         // terminate MPI computation

}



// --------------------------------//
// author: Jan Frederik Engels
// contributors: Julius Plehn
// date: June 2024
// --------------------------------//

#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

// compile using (for example):
// mpicc main-mpi.c -o mpi.x -lm

// definition of constants
// some values are given in meter
// some in seconds
//
#define x_0 10000
#define delta_x 1
#define coeff_sech 0.01
// depth of sea at deepest point
#define b_0 1000
// gravitational acceleration
#define g 10
#define delta_t 0.001

double *h;
double *h1;
double *h2;
double *u;
double *u1;
double *u2;
long local_npoints;
long local_start, local_end;
long start_index, end_index;

int mpi_size, mpi_rank;

enum halo { HALO_NONE = 0, HALO_LEFT = 1, HALO_RIGHT = 2 };
enum halo halo_property;

long count_points() { return (int)(2 * x_0 / delta_x); }

double get_x(long i) {
    if (halo_property & HALO_LEFT) {
        i = local_start + i - 1;
    } else {
        i = local_start + i;
    }
    return i * delta_x - x_0;
}

double waterdepth(long i) {
    double x = get_x(i);
    return b_0 * (x * x / (x_0 * x_0) - 1);
}

void initialise_h(double *h) {
    double x, tmp;
    long start_index_init = 0;
    long end_index_init = end_index;
    if (halo_property & HALO_LEFT)
        start_index_init = 1;
    if (mpi_rank == mpi_size - 1)
        end_index_init += 1;

    for (long i = start_index_init; i <= end_index_init; i++) {
        x = get_x(i);
        tmp = 2 / (exp(coeff_sech * x) + exp(-coeff_sech * x));
        if (tmp > 1e-10)
            h[i] = tmp * tmp;
        else
            h[i] = 0;
    }
}

void initialise_u(double *u) {
    long start_index_init = 0;
    if (halo_property & HALO_LEFT)
        start_index_init = 1;

    for (long i = start_index_init; i <= end_index; i++) {
        u[i] = 0;
    }
}

void print_values(FILE *data, double *h, double *u) {
    long i;
    fprintf(data, "#x\th\tu\tb\n");
    for (i = start_index; i < end_index; i++) {
        fprintf(data, "%g\t%g\t%g\t%g\n", get_x(i), h[i], u[i],
                waterdepth(local_start + i));
    }
}

double lu(double *h, double *u, long i) {
    return u[i] * (u[i + 1] - u[i - 1]) + g * (h[i + 1] - h[i - 1]);
}

double lh(double *h, double *u, long i) {
    return u[i] * (h[i + 1] - h[i - 1]) -
           u[i] * (waterdepth(i + 1) - waterdepth(i - 1)) +
           (h[i] - waterdepth(i)) * (u[i + 1] - u[i - 1]);
}

// sets h1,u1 using values (startconditions) from u2/h2
void euler(double *h1, double *h2, double *u1, double *u2) {
    long i;

    for (i = start_index; i <= end_index; i++) {
        u1[i] = u2[i] - (delta_t / (2. * delta_x)) * lu(h2, u2, i);
        h1[i] = h2[i] - (delta_t / (2. * delta_x)) * lh(h2, u2, i);
    }
}

long findmax(double *h, long oldmaxi, long outputmth) {
    long i, maxi = oldmaxi - 10;
    for (i = oldmaxi - 10;
         (i < oldmaxi + (int)(outputmth / delta_x) + 30) && (i < end_index);
         i++) {
        if (h[i] > h[maxi]) {
            maxi = i;
        }
    }
    return maxi;
}

double minval(double *h) {
    double hmin = 1000000;
    for (long i = start_index; i <= end_index; i++) {
        if (h[i] < hmin)
            hmin = h[i];
    }
    return hmin;
}

double maxval(double *h) {
    double hmax = -10;
    for (long i = start_index; i <= end_index; i++) {
        if (h[i] > hmax)
            hmax = h[i];
    }
    return hmax;
}

double ng_derivative(double xi, double xi_1, double deltax) {
    return (xi - xi_1) / deltax;
}

void swap(double **var, double **var1, double **var2) {
    double *temp;

    temp = *var2;
    *var2 = *var1;
    *var1 = *var;
    *var = temp;
}

void set_boundaries(double *var) {

    long npoints = local_npoints;
    if (mpi_rank == 0) {
        var[0] = 0;
    }
    if (mpi_rank == mpi_size - 1) {
        var[npoints] = 0;
    }
}

// Exchange h2 halos
void exchange_halos(double *var) {
    if (mpi_size > 1) {
        if (mpi_rank % 2 == 0) {
            if (mpi_rank != mpi_size - 1) {
                // Send to the right neighbor
                int index = local_npoints - 1;
                if (mpi_rank > 0)
                    index += 1;
                MPI_Send(&var[index], 1, MPI_DOUBLE, mpi_rank + 1, 1,
                         MPI_COMM_WORLD);

                // Receive from the right
                index = local_npoints;
                if (mpi_rank > 0)
                    index += 1;
                MPI_Recv(&var[index], 1, MPI_DOUBLE, mpi_rank + 1, 1,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
            if (mpi_rank != 0) {
                // Send to the left neighbor
                MPI_Send(&var[1], 1, MPI_DOUBLE, mpi_rank - 1, 1,
                         MPI_COMM_WORLD);
                // Receive from the left
                MPI_Recv(&var[0], 1, MPI_DOUBLE, mpi_rank - 1, 1,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        } else {
            if (mpi_rank != mpi_size - 1) {
                // Receive from the right
                int index = local_npoints;
                if (mpi_rank > 0)
                    index += 1;
                MPI_Recv(&var[index], 1, MPI_DOUBLE, mpi_rank + 1, 1,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                // Send to the right neighbor
                index = local_npoints - 1;
                if (mpi_rank > 0)
                    index += 1;
                MPI_Send(&var[index], 1, MPI_DOUBLE, mpi_rank + 1, 1,
                         MPI_COMM_WORLD);
            }
            // Receive from the left
            MPI_Recv(&var[0], 1, MPI_DOUBLE, mpi_rank - 1, 1, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            //  Send to the left neighbor
            MPI_Send(&var[1], 1, MPI_DOUBLE, mpi_rank - 1, 1, MPI_COMM_WORLD);
        }
    }
}

void print_var(double *var, int count, int rank) {
    if (mpi_rank == rank) {
        printf("Rank: %d | ", rank);
        for (int i = 0; i < count; ++i) {
            printf("%g ", var[i]);
        }
        printf("\n");
    }
}

int main(int argc, char *argv) {
    MPI_Init(NULL, NULL);

    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    if (mpi_rank == 0) {
        printf("Running with %d MPI processes\n", mpi_size);
    }

    long i;
    long npoints = count_points();

    if (mpi_rank == 0) {
        printf("npoints: %d\n", npoints);
    }

    // Figure out halos
    int added_halos;
    if (mpi_size > 1) {
        if (mpi_rank == 0) {
            halo_property = HALO_RIGHT;
            added_halos = 1;
        } else if (mpi_rank == mpi_size - 1) {
            halo_property = HALO_LEFT;
            added_halos = 1;
        } else {
            halo_property = HALO_LEFT | HALO_RIGHT;
            added_halos = 2;
        }
    } else {
        halo_property = HALO_NONE;
        added_halos = 0;
    }

    // Decomposition
    local_npoints = npoints / mpi_size;
    int remainder = npoints % mpi_size;

    start_index = 1;

    if (mpi_rank < remainder) {
        local_start = mpi_rank * (local_npoints + 1);
        local_end = local_start + local_npoints;
        local_npoints += 1;
    } else {
        local_start = mpi_rank * local_npoints + remainder;
        local_end = local_start + (local_npoints - 1);
    }

    // Specify ranges once and take halos into consideration
    end_index = local_npoints - 1;
    if (halo_property & HALO_LEFT && halo_property & HALO_RIGHT) {
        end_index = local_npoints;
    }

    h = malloc(sizeof(double) * (local_npoints + added_halos));
    h1 = malloc(sizeof(double) * (local_npoints + added_halos));
    h2 = malloc(sizeof(double) * (local_npoints + added_halos));

    u = malloc(sizeof(double) * (local_npoints + added_halos));
    u1 = malloc(sizeof(double) * (local_npoints + added_halos));
    u2 = malloc(sizeof(double) * (local_npoints + added_halos));

    if (h == NULL || h1 == NULL || h2 == NULL || u == NULL || u1 == NULL ||
        u2 == NULL) {
        fprintf(stderr, "Insufficient memory");
        exit(23);
    }

    long maxi = ((npoints) / 2) + 5;
    long oldmaxi;
    long oldmaxi_output = (npoints) / 2;
    int maximth = 20;
    double t = 0.0;
    long n = 0;
    int outputmth = 1000; // write every mth set of values to stderr
    int dataqth = 200;    // write every qth set of values to output#
    char file[60];        // char pointer for filename
    FILE *out;
    double *temp;
    int first = 1;
    double before, after, duration;

    initialise_u(u2);
    initialise_h(h2);

    exchange_halos(h2);
    exchange_halos(u2);

    euler(h1, h2, u1, u2);

    if (mpi_rank == 0)
        fprintf(stderr, "#time	h_max\n");

    before = MPI_Wtime();
    while (t < 50) {
        if (!first) {
            swap(&h, &h1, &h2);
            swap(&u, &u1, &u2);
        } else {
            first = 0;
        }

        exchange_halos(h1);
        exchange_halos(u1);

        set_boundaries(u);
        set_boundaries(h);

        for (long i = 1; i <= end_index; i++) {
            u[i] = u2[i] - (delta_t / delta_x) * lu(h1, u1, i);
            h[i] = h2[i] - (delta_t / delta_x) * lh(h1, u1, i);
        }

        t += delta_t;
        if ((n % maximth) == 0) {

            if ((n % outputmth) == 0) {
                // HOMEWORK: replace maxval with minval
                double maxh = maxval(h);
                double global_max;
                // HOMEWORK: adapt MPI_Reduce to compute the MIN
                MPI_Reduce(&maxh, &global_max, 1, MPI_DOUBLE, MPI_MAX, 0,
                           MPI_COMM_WORLD);

                if (mpi_rank == 0)
                    fprintf(stderr, "%g\t%g\n", t, global_max);
            }
        }

        n++;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    after = MPI_Wtime();
    duration = after - before;
    if (mpi_rank == 0)
        printf("Run took %g seconds.\n", duration);

    // print_values(stdout, h, u);

    MPI_Finalize();

    // deallocate memory
    free(h);
    free(h1);
    free(h2);
    free(u);
    free(u1);
    free(u2);

    return 0;
}

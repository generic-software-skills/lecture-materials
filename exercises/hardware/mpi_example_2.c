#include <stdio.h>
#include <mpi.h>           // include the header for the MPI library

// Find the maximum of numbers in a vector
double find_maximum( double * vector, int N ){
   double max = 0;
// TODO: Create a new variable for the global maximum, look below for the name.

   for( int i=0; i<N; i++){
      if( vector[i] > max ){
         max = vector[i];
      }
   }

// TODO: Use MPI_Allreduce to find the maximum over all the ranks
// MPI_Allreduce(const void *sendbuf, void *recvbuf, int count,
//               MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
   // sendbuf:  of what do we want to compute the maximum?
   // recvbuf:  Where should the global maximum be written? Do we need a new
   //           variable for this?
   // count:    How many variables do we want to exchange?
   // datatype: MPI_DOUBLE
   // op:       MPI_MAX
   // comm:     Which communicator contains all ranks? The right one is in use here.
   // Summing up:
//  MPI_Allreduce(&max, &globalmax, ..... );

// TODO: return the global maximum
   return max;
}

int main(int argc, char** argv){

   int no_of_ranks, my_rank;
   int n = 1024;
   double vector[n];
   double max;
   double my_first;

   MPI_Init(&argc, &argv); // initiate MPI computation

   MPI_Comm_size(MPI_COMM_WORLD, &no_of_ranks); // get number of MPI ranks

   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);  // get rank of this process

   // Each rank will have n numbers,
   // starting from where the previous left off
   my_first = n * my_rank;

   // Generate a vector
   for( int i=0; i<n; i++){
      double tmp = my_first + i - n*no_of_ranks;
      vector[i] = tmp*tmp;
   }

   //Find the maximum and print
   max = find_maximum( vector, n );
   printf("The largest number is %f\n", max);

   MPI_Finalize();         // terminate MPI computation

}


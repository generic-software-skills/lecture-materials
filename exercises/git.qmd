---
title: "Git"
---

Think about different occasions in your studies or work in which Git may help
you. Are there any limitations?  Summarise your thoughts in a text file and add
it to your personal course repository.

If you already have some existing scripts/code from you day-to-day work, you
may want to add those to a git repository and refer to it.

Next week we will randomly select some participants to discuss their thoughts.

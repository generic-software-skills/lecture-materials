from git2 import SimpleGit

sg = SimpleGit("testrepo/.git")

FILE = "100644"
FOLDER = "40000"

sg.init()
bv1 = sg.put_blob(b"version 1\n")
t1 = sg.put_tree(
    [
        (FILE, "test.txt", bv1),
    ]
)
c1 = sg.commit(t1, "Tobias Kölling <tobias.koelling@mpimet.mpg.de>", "first commit")

bv2 = sg.put_blob(b"version 2\n")
bnew = sg.put_blob(b"new file\n")
t2 = sg.put_tree(
    [
        (FILE, "test.txt", bv2),
        (FILE, "new.txt", bnew),
    ]
)
c2 = sg.commit(
    t2, "Tobias Kölling <tobias.koelling@mpimet.mpg.de>", "second commit", parents=[c1]
)

t3 = sg.put_tree(
    [
        (FILE, "test.txt", bv2),
        (FILE, "new.txt", bnew),
        (FOLDER, "bak", t1),
    ]
)
c3 = sg.commit(
    t3, "Tobias Kölling <tobias.koelling@mpimet.mpg.de>", "third commit", parents=[c2]
)

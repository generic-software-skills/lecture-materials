import zlib
from pathlib import Path
from hashlib import sha1

from appendix import make_blob_object, make_tree_object, make_commit_object


class SimpleGit:
    def __init__(self, root):
        self.root = Path(root)

    def init(self) -> None:
        (self.root / "refs" / "heads").mkdir(parents=True)
        (self.root / "HEAD").write_text("ref: refs/heads/main")

    def head_ref(self):
        head = (self.root / "HEAD").read_text()
        assert head.startswith("ref: ")
        return head.split(" ", 1)[-1]

    def update_current_ref(self, new_hash) -> None:
        (self.root / self.head_ref()).write_text(new_hash.hexdigest())

    def object_path(self, h) -> Path:
        h = h.hexdigest()
        return self.root / "objects" / h[:2] / h[2:]

    def put_object(self, git_object: bytes) -> str:
        h = sha1(git_object)
        p = self.object_path(h)
        p.parent.mkdir(parents=True, exist_ok=True)
        p.write_bytes(zlib.compress(git_object))
        return h

    def get_object(self, h: str) -> bytes:
        return zlib.decompress(self.object_path(h).read_bytes())

    def put_blob(self, contents: bytes) -> str:
        return self.put_object(make_blob_object(contents))

    def put_tree(self, items):
        return self.put_object(make_tree_object(items))

    def commit(
        self,
        tree,
        author,
        message,
        parents=None,
        committer=None,
        author_time=None,
        committer_time=None,
    ):
        h = self.put_object(
            make_commit_object(
                tree, author, message, parents, committer, author_time, committer_time
            )
        )
        self.update_current_ref(h)
        return h

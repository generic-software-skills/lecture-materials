#!/bin/bash

set -e
set -x

rm -rf testrepo
jupytext --to py appendix.qmd
python3 test_git2.py
cd testrepo
git status
git log

variables:
  GIT_DEPTH: 50
  QUARTO_VERSION: 1.4.553

workflow:
  # these rules ensure that whenever there's a merge request, a "merge request pipeline" is run,
  # but otherwise a classical branch-pipeline is run. This enables the use of merge-request specific
  # environment variables within CI.
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

stages:
  - test
  - build

run_pre_commit_hooks:
  tags:
    - conda
  stage: test
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  cache:
    paths:
      - ${PRE_COMMIT_HOME}
  script:
    - python3 -V  # Print out python version for debugging
    - python3 -m pip install pre-commit
    - echo running pre-commit for revision range $COMPARE_SOURCE ... HEAD
    - pre-commit run --from-ref $COMPARE_SOURCE --to-ref HEAD
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        COMPARE_SOURCE: $CI_MERGE_REQUEST_DIFF_BASE_SHA
    - if: '$CI_COMMIT_BRANCH'
      variables:
        COMPARE_SOURCE: HEAD^1

.build: &build
  stage: build
  tags:
    - conda
  variables:
    QUARTODEB: quarto_cache/quarto-${QUARTO_VERSION}.deb
  cache:
    - key: cache-default
      paths:
       - .jupyter_cache
       - quarto_cache
  before_script:
    - mkdir -p quarto_cache
    - if [ ! -f ${QUARTODEB} ] ; then wget "https://github.com/quarto-dev/quarto-cli/releases/download/v${QUARTO_VERSION}/quarto-${QUARTO_VERSION}-linux-amd64.deb" -O  ${QUARTODEB} ; fi
    - dpkg -i ${QUARTODEB}
    - python3 -m pip install -r requirements.txt
    - quarto check install
    - quarto check jupyter
  script:
    - ./make_presentations
  artifacts:
    paths:
      - public
    expire_in: 20min

preview:
  extends: .build
  variables:
    DEPLOY_URL: https://$CI_PROJECT_NAMESPACE.$CI_PAGES_DOMAIN/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public
    ICAL_URL: webcal://$CI_PROJECT_NAMESPACE.$CI_PAGES_DOMAIN/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/lectures.ics
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: $DEPLOY_URL/index.html
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

pages:
  extends: .build
  variables:
    DEPLOY_URL: https://$CI_PROJECT_NAMESPACE.$CI_PAGES_DOMAIN/$CI_PROJECT_NAME
    ICAL_URL: webcal://$CI_PROJECT_NAMESPACE.$CI_PAGES_DOMAIN/$CI_PROJECT_NAME/lectures.ics
  environment:
    name: production
    url: $DEPLOY_URL
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
